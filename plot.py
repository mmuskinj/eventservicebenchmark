#!/usr/bin/env python3

import re
import json
import sys
import datetime as dt
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from dateutil import parser
from matplotlib.collections import PolyCollection

inputFile = open(sys.argv[1]).readline()
N_ATHENAS = inputFile.count('Actor')
if not N_ATHENAS:
    N_ATHENAS = inputFile.count('Athena')
N_CPU = max([int(x) for x in re.findall('worker_([0-9]*)', inputFile)]) + 1

print ("N_ATHENAS ", N_ATHENAS)
print ("N_CPU ", N_CPU)

MAX_ATHENA_PRINT = N_ATHENAS
if len(sys.argv) > 2:
    MAX_ATHENA_PRINT = int(sys.argv[2])

JOBS = []


LINEWIDTH = 0

data = []
cats = {}
colormapping = {}

workers = []

N_MERGE_JOBS = 0

job = None
with open(sys.argv[1]) as json_file:
    job = json.load(json_file)

PandaID = None
for k in job:
    if PandaID != None:
        print (f"Unexpected key in job: {key}")
    PandaID = k

# Ran over midnight
job_start_time = parser.parse(job[PandaID]['start time'])
job_stop_time = parser.parse(job[PandaID]['stop time'])
midnight = job_stop_time.hour < job_start_time.hour

# job cancel time
job_cancel_time = None
if 'canceled time' in job[PandaID]:
    job_cancel_time = parser.parse(job[PandaID]['canceled time'])

# driver start time
driver_start_time = parser.parse(job[PandaID]['driver start time'])

for actor in job[PandaID]:
    if "Merged" in actor:
        continue
    if 'start time' in actor:
        continue
    if len(JOBS) >= MAX_ATHENA_PRINT:
        continue
    JOBS += [actor]

print (f"job start time: {job_start_time}")
print (f"job stop time: {job_stop_time}")
print (f"job cancel time: {job_cancel_time}")
print (f"driver start time: {driver_start_time}")
print (f"PandaID: {PandaID}")
print ("MAX_ATHENA_PRINT ", MAX_ATHENA_PRINT)
print ("JOBS ", len(JOBS))

for athenaID, jobName in enumerate(JOBS):
    startTime = ""
    for key in job[PandaID][jobName]:
        name = jobName + "_" + key

        # pilot
        if key == 'pilot':
            start_time = parser.parse(job[PandaID][jobName][key]['start time'])
            start_payload = parser.parse(
                job[PandaID][jobName][key]['payload time'])
            data += [
                (start_time, start_payload, name)
            ]
            colormapping[name] = 'red'
            print("Pilot init ", start_time, start_payload)
            continue

        # Athena init
        if key == 'init':
            start_time = parser.parse(job[PandaID][jobName][key]['start time'])
            stop_time = parser.parse(job[PandaID][jobName][key]['stop time'])
            print("Athena init ", start_time, stop_time)
            data += [
                (start_time, stop_time, name)
            ]
            colormapping[name] = 'navy'
            if 'Merged' in jobName:
                cats[name] = -(N_MERGE_JOBS+1)
                N_MERGE_JOBS += 1
                colormapping[name] = 'xkcd:shamrock'
            continue

        # Workers
        workers += [name]
        workerID = int(re.findall('worker_([0-9]+)', key)[0])
        cats[name] = athenaID*N_CPU + workerID
        colormapping[name] = 'xkcd:blue'
        for key2, event in job[PandaID][jobName][key].items():
            if 'init' in key2:
                name_event = name + '_init'
                colormapping[name_event] = 'xkcd:greenish yellow'
            if 'start time' not in event.keys():
                continue
            start_time = parser.parse(event['start time'])
            # print (start_time)
            if 'stop time' in event.keys():
                stop_time = parser.parse(event['stop time'])
                name_event = name
            else:
                stop_time = job_stop_time
                name_event = name + '_unfinished'
                colormapping[name_event] = 'greenyellow'
            data += [
                (start_time, stop_time, name_event)
            ]

verts = []
colors = []
colors_e = []

# Start time
v = [
    ((job_start_time-job_start_time).total_seconds()/60., 0-.4),
    ((job_start_time-job_start_time).total_seconds()/60., (N_CPU*len(JOBS))+.4),
    ((driver_start_time-job_start_time).total_seconds()/60., (N_CPU*len(JOBS))+.4),
    ((driver_start_time-job_start_time).total_seconds()/60., 0-.4),
]
verts.append(v)
colors.append('grey')
colors_e.append('grey')

# cancel time
if job_cancel_time:
    v = [
        ((job_cancel_time-job_start_time).total_seconds()/60., 0-.4),
        ((job_cancel_time-job_start_time).total_seconds()/60., (N_CPU*len(JOBS))+.4),
        ((job_cancel_time-job_start_time).total_seconds() /
         60. + 0.5, (N_CPU*len(JOBS))+.4),
        ((job_cancel_time-job_start_time).total_seconds()/60. + 0.5, 0-.4),
    ]
    verts.append(v)
    colors.append('black')
    colors_e.append('black')

# Athena
for d in data:
    if 'pilot' in d[2]:
        MAX = N_CPU - 1
        while d[2].replace('pilot', 'worker_%s' % (MAX)) not in cats:
            MAX -= 1
        v = [
            ((d[0]-job_start_time).total_seconds()/60.,
             cats[d[2].replace('pilot', 'worker_0')]-.4),
            ((d[0]-job_start_time).total_seconds()/60.,
             cats[d[2].replace('pilot', 'worker_%s' % (MAX))]+.4),
            ((d[1]-job_start_time).total_seconds()/60.,
             cats[d[2].replace('pilot', 'worker_%s' % (MAX))]+.4),
            ((d[1]-job_start_time).total_seconds()/60.,
             cats[d[2].replace('pilot', 'worker_0')]-.4),
        ]
    elif 'init' in d[2] and 'Merged' not in d[2] and 'worker' not in d[2]:
        MAX = N_CPU - 1
        while d[2].replace('init', 'worker_%s' % (MAX)) not in cats:
            MAX -= 1
        v = [
            ((d[0]-job_start_time).total_seconds()/60.,
             cats[d[2].replace('init', 'worker_0')]-.4),
            ((d[0]-job_start_time).total_seconds()/60.,
             cats[d[2].replace('init', 'worker_%s' % (MAX))]+.4),
            ((d[1]-job_start_time).total_seconds()/60.,
             cats[d[2].replace('init', 'worker_%s' % (MAX))]+.4),
            ((d[1]-job_start_time).total_seconds()/60.,
             cats[d[2].replace('init', 'worker_0')]-.4),
        ]
    elif 'init' in d[2] and 'Merged' not in d[2] and 'worker' in d[2]:
        v = [
            ((d[0]-job_start_time).total_seconds() /
             60., cats[d[2].replace("_init", "")]-.4),
            ((d[0]-job_start_time).total_seconds() /
             60., cats[d[2].replace("_init", "")]+.4),
            ((d[1]-job_start_time).total_seconds() /
             60., cats[d[2].replace("_init", "")]+.4),
            ((d[1]-job_start_time).total_seconds() /
             60., cats[d[2].replace("_init", "")]-.4),
        ]
    elif 'init' in d[2] and 'Merged' in d[2]:
        v = [
            ((d[0]-job_start_time).total_seconds()/60., cats[d[2]]-.4),
            ((d[0]-job_start_time).total_seconds()/60., cats[d[2]]+.4),
            ((d[1]-job_start_time).total_seconds()/60., cats[d[2]]+.4),
            ((d[1]-job_start_time).total_seconds()/60., cats[d[2]]-.4),
        ]
    elif 'unfinished' in d[2]:
        v = [
            ((d[0]-job_start_time).total_seconds()/60.,
             cats[d[2].replace("_unfinished", "")]-.4),
            ((d[0]-job_start_time).total_seconds()/60.,
             cats[d[2].replace("_unfinished", "")]+.4),
            ((d[1]-job_start_time).total_seconds()/60.,
             cats[d[2].replace("_unfinished", "")]+.4),
            ((d[1]-job_start_time).total_seconds()/60.,
             cats[d[2].replace("_unfinished", "")]-.4),
        ]
    else:
        v = [
            ((d[0]-job_start_time).total_seconds()/60., cats[d[2]]-.4),
            ((d[0]-job_start_time).total_seconds()/60., cats[d[2]]+.4),
            ((d[1]-job_start_time).total_seconds()/60., cats[d[2]]+.4),
            ((d[1]-job_start_time).total_seconds()/60., cats[d[2]]-.4),
        ]
    verts.append(v)
    colors.append(colormapping[d[2]])
    colors_e.append(colormapping[d[2]])

black = (0, 0, 0, 1)
bars = PolyCollection(verts, facecolors=colors,
                      edgecolors=colors_e,  linewidth=LINEWIDTH)


fig, ax = plt.subplots()
ax.add_collection(bars)
ax.autoscale()

plt.xlabel("time [min]")

yticks = []
yticklabels = []
for k, v in cats.items():
    if v < 0:
        print(k, " ", v)
    yticks += [v]
    yticklabels += [k]

ax.set_yticks([])
ax.set_yticklabels([])

plt.yticks(fontsize=4)
plt.savefig(sys.argv[1].replace(".json", "") + "_%s_actors.pdf" % (
    MAX_ATHENA_PRINT), bbox_inches='tight')
