#!/usr/bin/env python
from dateutil import parser
from matplotlib import pyplot as plt
import os


def main(options, args):
    time = []
    events = []
    events_per_time = []
    events_at_fixed_time = []
    time_intervals = []
    events_per_fixed_time = []
    time_interval = 5.0  # minutes
    num_intervals = 0
    f = open(options.input, 'r')
    event_range_status = False
    finished_events = False
    start_time = None
    for l in f:
        if "Event ranges status for job" in l:
            event_range_status = True
        elif event_range_status and "Finished" in l:
            finished_events = True
            num_events = l.split("Finished:")[1]
            if len(events):
                delta_events = int(num_events) - events[-1]
            else:
                delta_events = int(num_events)
            events += [int(num_events)]
            events_per_time += [delta_events]
        elif finished_events:
            date = l.split("|")[1]
            current_time = parser.parse(date)
            if not start_time:
                start_time = current_time
            delta_time = (current_time - start_time).total_seconds()/60.
            time += [delta_time]
            if delta_time//time_interval > num_intervals:
                if not len(time_intervals):
                    time_diff = delta_time
                    event_diff = events[-1]
                else:
                    time_diff = delta_time - time_intervals[-1]
                    event_diff = events[-1] - events_at_fixed_time[-1]
                time_intervals += [delta_time]
                events_at_fixed_time += [events[-1]]
                events_per_fixed_time += [event_diff/time_diff]
                num_intervals += 1
            event_range_status = False
            finished_events = False

    job_id = os.path.dirname(options.input).split("/")[-1]

    plt.figure()
    plt.plot(time, events)
    plt.xlabel('time [min.]')
    plt.ylabel('processed events')
    plt.savefig(f'integral_{job_id}.pdf')

    plt.figure()
    plt.plot(time, events_per_time, linestyle='-', marker='o')
    plt.xlabel('time [min.]')
    plt.ylabel('processed events / point')
    plt.savefig(f'events_per_time_{job_id}.pdf')

    plt.figure()
    plt.plot(time_intervals, events_per_fixed_time, marker='o')
    plt.xlabel('time [min.]')
    plt.ylabel(f'event rate / ({time_interval} min.) [min$^{"{-1}"}$]')
    plt.savefig(f'event_rate_{job_id}.pdf')


if __name__ == "__main__":
    import optparse
    optionParser = optparse.OptionParser()

    # ----------------------------------------------------
    # arguments
    # ----------------------------------------------------
    optionParser.add_option('-i', '--input',
                            action="store", dest="input",
                            help="raythena log file path")

    # parse input arguments
    options, args = optionParser.parse_args()

    # main
    main(options, args)
